<?php
//
// Менеджер статей
//
class M_Table
{
	private static $instance; 	// ссылка на экземпляр класса
	private $msql; 				// драйвер БД
	
	protected $table;
	protected $idField = 'id';
	
	//
	// Получение единственного экземпляра (одиночка)
	//
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new self();
		
		return self::$instance;
	}

	//
	// Конструктор
	//
	private function __construct()
	{
		$this->msql = M_MSQL::Instance(); //M_PGSQL::Instance();
	}
	
	//
	// Список всех статей
	//
	public function All()
	{
		$query = "SELECT * 
				  FROM $this->table 
				  ORDER BY id_article DESC";
				  
		return $this->msql->Select($query);
	}

	//
	// Конкретная статья
	//
	public function Get($id)
	{
		// Запрос.
		$t = "SELECT * 
			  FROM $this->table 
			  WHERE $this->idField = '%d'";
			  
		$query = sprintf($t, $id);
		$result = $this->msql->Select($query);
		return $result[0];
	}

	//
	// Добавить статью
	//
	public function Add($obj)
	{	
		$this->msql->Insert($this->table, $obj);
		return true;
	}

	//
	// Изменить статью
	//
	public function Edit($id, $obj)
	{
		$t = "$this->idField = '%d'";		
		$where = sprintf($t, $id);		
		$this->msql->Update($this->table, $obj, $where);
		return true;
	}

	//
	// Удалить статью
	//
	public function Delete($id)
	{
		// Запрос.
		$t = "$this->idField  = '%d'";		
		$where = sprintf($t, $id);		
		$this->msql->Delete($this->table, $where);
		return true;
	}
}
