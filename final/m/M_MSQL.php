<?php
//
// Помощник работы с БД
//
include_once 'dbconfig.php';

class M_MSQL
{
	private static $instance;
	
	protected $link;
	
	public static function Instance()
	{
		if (self::$instance == null)
			self::$instance = new M_MSQL();
		
		return self::$instance;
	}
	
	private function __construct()
	{
		$this->link = new mysqli(DB_HOST, DB_USER, DB_PASS, DB);
		$this->link->set_charset('utf8');
		echo $this->link->character_set_name ();
	}
	
	//
	// Выборка строк
	// $query    	- полный текст SQL запроса
	// результат	- массив выбранных объектов
	//
	public function Select($query)
	{
		$result = $this->link->query($query); //mysqli_query($this->link, $query);
		
		if ($this->link->error)
			throw new DBException($this->link->error);
		
		$n = $result->num_rows;
		$arr = array();
	
		for($i = 0; $i < $n; $i++)
		{
			$row = $result->fetch_assoc();		
			$arr[] = $row;
		}

		return $arr;				
	}
	
	//
	// Вставка строки
	// $table 		- имя таблицы
	// $object 		- ассоциативный массив с парами вида "имя столбца - значение"
	// результат	- идентификатор новой строки
	//
	public function Insert($table, $object)
	{			
		$columns = array(); 
		$values = array(); 
	
		foreach ($object as $key => $value)
		{
			$key = $this->link->real_escape_string($key . '');
			$columns[] = $key;
			
			if ($value === null) {
				$values[] = 'NULL';
			} else {
				$value = $this->link->real_escape_string($value . '');							
				$values[] = "'$value'";
			}
		}

		$columns_s = implode(',', $columns); 
		$values_s = implode(',', $values);  
			
		$query = "INSERT INTO $table ($columns_s) VALUES ($values_s)";
		$result = $this->link->query($query);
								
		if ($this->link->error)
			throw new DBException($this->link->error);
			
		return $this->link->insert_id;
	}
	
	//
	// Изменение строк
	// $table 		- имя таблицы
	// $object 		- ассоциативный массив с парами вида "имя столбца - значение"
	// $where		- условие (часть SQL запроса)
	// результат	- число измененных строк
	//	
	public function Update($table, $object, $where)
	{
		$sets = array();
	
		foreach ($object as $key => $value)
		{
			$key = $this->link->real_escape_string($key . '');
			
			if ($value === null)
			{
				$sets[] = "$key=NULL";			
			}
			else
			{
				$value = $this->link->real_escape_string($value . '');					
				$sets[] = "$key='$value'";			
			}			
		}

		$sets_s = implode(',', $sets);			
		$query = "UPDATE $table SET $sets_s WHERE $where";
		$this->link->query($query);
		
		if ($this->link->error)
			throw new DBException($this->link->error);

		return $this->link->affected_rows;
	}
	
	//
	// Удаление строк
	// $table 		- имя таблицы
	// $where		- условие (часть SQL запроса)	
	// результат	- число удаленных строк
	//		
	public function Delete($table, $where)
	{
		$query = "DELETE FROM $table WHERE $where";		
		$this->link->query($query);
						
		if ($this->link->error)
			throw new DBException($this->link->error);

		return $this->link->affected_rows;	
	}
}
