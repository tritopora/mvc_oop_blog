<?php
//
// Базовый контроллер сайта.
//
abstract class C_Base extends C_Controller
{
	protected $title_h1;		// заголовок страницы
	protected $content;		// содержание страницы

	//
	// Конструктор.
	//
	function __construct()
	{		
	}
	
	protected function before()
	{
		$this->title_h1 = 'Мой блог';
		$this->content = '';
		$this->comments = '';
	}
	
	//
	// Генерация базового шаблонаы
	//	
	public function render()
	{
		$vars = array(
			'title_h1' => $this->title_h1, 
			'content' => $this->content, 
			'comments' => $this->comments 
			);	
		$page = $this->Template('v/v_base.php', $vars);				
		echo $page;

	/*	$vars = array( 'comments' => $this->comments);	
		$page1 = $this->Template('v/v_comments.php', $vars);				
		echo $page1;
		*/
	}	
}
