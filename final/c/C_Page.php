<?php
//
// Конттроллер страницы чтения.
//
spl_autoload_register('my_autoloader');

class C_Page extends C_Base
{	
	protected $All_full;
	protected $Only;
	protected $delete;
	protected $All;
	protected $Add;
	protected $edit_Only;
	protected $All_comments;

	protected function action_index(){
		$mArticles = M_Article::Instance();
		$this->All_full = $mArticles->All_full();
		$this->title_h1 .= '::Все статьи на главной';
	 	$this->content = $this->Template('v/v_index.php', array('variable' => $this->All_full));
	}

	protected function action_article(){
		$mArticles = M_Article::Instance();
		$this->Only = $mArticles->Only($_GET['id']);
		//print_r($this->Only);
		$this->title_h1 .= '::Выбор одной статьи';	

		$m_comments = M_Comments::Instance();
		$this->All_comments = $m_comments->All_comments($_GET['id']);
	
		if($_POST){
			$this->Add_comments = $m_comments->Add_comments( 
			$this->Only['id'], 
			$_POST['login'], 
			$_POST['text'], 
			$_POST['created_date']
			);
		}else{
			$this->Add_comments = '';
		}		

	 	$this->content = $this->Template('v/v_article.php', array(
	 		'article' => $this->Only, 
	 		'comments' => $this->All_comments,
	 		'Add_comments' => $this->Add_comments
	 		));
		
		
		//print_r($this->All_coments);
	 	//$this->comments = $this->Template('v/v_comments.php', array('comments' => $this->All_comments));

	}

	protected function action_delete(){
		$mArticles = M_Article::Instance();
		$this->delete = $mArticles->delete($_GET['id']);
		$this->title_h1 .= '::Вы удалили статью ' . $_GET['id'];			
	 	$this->content = $this->Template('v/v_delete.php', array('variable' => $this->delete));
	}
	
	protected function action_editor(){
		$mArticles = M_Article::Instance();
		$this->All = $mArticles->All();
		$this->title_h1 .= '::Редактирование статей';
		$this->content = $this->Template('v/v_editor.php',array('variable' => $this->All));
	}

	protected function action_new(){	
		$mArticles = M_Article::Instance();
		
		//echo count($_POST);

			if (!empty($_POST['title'] & $_POST['text']))
			{
				$mArticles->Add($_POST['title'], $_POST['text'], '3');
				$articles['error'] = false;
				header('Location: index.php?c=editor&act=editor');
				die();
			}
			else
			{
				$articles['title'] = $_POST['title'];
				$articles['text']  = $_POST['text'];
				$articles['error'] = true;
			}

		$this->title_h1 .= '::Новая статья';
		$this->content = $this->Template('v/v_new.php', $articles);
	 	//var_dump($this->content) ;
	}

	protected function action_edit(){		
		$mArticles = M_Article::Instance();
		$this->edit_Only =	$mArticles->edit_Only();
		$this->title_h1 .= '::Редактирование выбраной статьи';						
		// Обработка отправки формы.
		if (!empty($_POST))
		{
			if ($mArticles->edit_post($_POST['title'], $_POST['content'], $_POST['date'], $_POST['autor']))
			{
				header('Location: index.php?c=editor&act=editor');
				die();
			}
			
			$title = $_POST['title'];
			$content = $_POST['content'];
			$error = true;
		}
		else
		{
			$title = '';
			$content = '';
			$error = false;
		}


		$this->content = $this->Template('v/v_edit.php', array('variable' => $this->edit_Only));		
	}

	
}
