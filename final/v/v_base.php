<?/*
Шаблон редактируемой страницы
=======================
$articles - список статей

статья:
id_article - идентификатор
title - заголвок
content - текст
*/?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Главная:все статьи!</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">	
	<link rel="stylesheet" type="text/css" media="screen" href="v/style.css" /> 
</head>
<body>

	<a href="index.php">Главная</a> | <a href="index.php?c=editor&act=editor">Консоль редактора</a>
	
	<hr/>
	
	<h1><?=$title_h1?></h1>
	
		<?=$content?>	
		
	<hr/>
	
	<small>
		<a href="http://prog-school.ru">Школа Программирования</a> &copy;
	</small>

</body>
</html>
